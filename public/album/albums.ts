export const Albums = [
  {
    name: "Cheerful cinimatics",
    artist: "Daddy's music",
    duration: 4.47,
    fileName: "cheerful-cinematic-song-without-solo-guitar-10709.mp3",
    imgUrl: "https://cdn.pixabay.com/audio/2022/06/30/13-58-30-235_200x200.png",
  },
  {
    name: "Chill lofi",
    artist: "VirtoSound",
    duration: 1.16,
    fileName: "chill-lofi-song-8444.mp3",
  },
  {
    name: "Dead by daylight",
    artist: "Tommy Muto",
    duration: 2.18,
    fileName: "dead-by-daylight-10243.mp3",
    imgUrl: "https://cdn.pixabay.com/audio/2022/09/16/10-14-24-334_200x200.jpg",
  },
  {
    name: "Let it go",
    artist: "ItWatR",
    duration: 2.13,
    fileName: "let-it-go-12279.mp3",
  },
];
