export enum HttpMethod {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
}

export enum ButtonSize {
  SM = "SM",
  MD = "MD",
  LG = "LG",
}
